/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "manager.h"
#include <QDebug>
#include <QThread>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

Manager::Manager(QObject *parent) : QObject(parent)
{
    started = false;
    fTemp = new QFile("/sys/class/hwmon/hwmon0/temp1_input");
    fTCore1 = new QFile("/sys/class/hwmon/hwmon0/temp2_input");
    fTCore2 = new QFile("/sys/class/hwmon/hwmon0/temp3_input");
    fTCore3 = new QFile("/sys/class/hwmon/hwmon0/temp4_input");
    fTCore4 = new QFile("/sys/class/hwmon/hwmon0/temp5_input");

    mValid = fTemp->open(QIODevice::ReadOnly) |
             fTCore1->open(QIODevice::ReadOnly) |
             fTCore2->open(QIODevice::ReadOnly) |
             fTCore3->open(QIODevice::ReadOnly) |
             fTCore4->open(QIODevice::ReadOnly);
    if(mValid) {
        tmCheckTemp = new QTimer(this);
        tmCheckTemp->setSingleShot(true);
        connect(tmCheckTemp, SIGNAL(timeout()), this, SLOT(checkTemperature()));
        tmCheckTemp->start(1000);
    }
    eCRegisters.fill(0, 256);
}

Manager::~Manager()
{
    if(fTemp->isOpen())
        fTemp->close();
    if(fTCore1->isOpen())
        fTCore1->close();
    if(fTCore2->isOpen())
        fTCore2->close();
    if(fTCore3->isOpen())
        fTCore3->close();
    if(fTCore4->isOpen())
        fTCore4->close();
    delete fTemp;
    delete fTCore1;
    delete fTCore2;
    delete fTCore3;
    delete fTCore4;
}

bool Manager::valid() const
{
    return mValid;
}

bool Manager::setMaxSpeed()
{
    bool ret = false;
    if(openIOPort()) {
        if(ecIntroSequence()) {
            if(!waitUntilMaskIsValue(0x02, 0x00)) {
                if(!(ret = writeByte(0x77, 0x68)))
                    emit error(tr("Error writing max speed command!"));
            } else {
                emit error(tr("Error waiting for writing magic value!"));
            }
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        } else {
            emit error(tr("Error intro EC sequence!"));
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        }
        if(!closeIOPort()) {
            ret = false;
            emit error(tr("Error closing port!"));
        }
    } else {
        emit error(tr("Error opening port!"));
    }
    return ret;
}

bool Manager::setNormalSpeed()
{
    bool ret = false;
    if(openIOPort()) {
        if(ecIntroSequence()) {
            if(!waitUntilMaskIsValue(0x02, 0x00)) {
                if(!(ret = writeByte(0x76, 0x68)))
                    emit error(tr("Error writing normal speed command!"));
            } else {
                emit error(tr("Error waiting for writing magic value!"));
            }
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        } else {
            emit error(tr("Error intro EC sequence!"));
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        }
        if(!closeIOPort()) {
            ret = false;
            emit error(tr("Error closing port!"));
        }
    } else {
        emit error(tr("Error opening port!"));
    }
    return ret;
}

void Manager::checkTemperature()
{
    if(!started){
        started = true;
        setNormalSpeed();
    }

    updateEcRegisters();
    int iTemp = 0;
    int tTemp = 0;
    QByteArray temp;

    if(fTemp->isOpen()) {
        int iTemp = 0;
        int tTemp = 0;

        fTemp->seek(0);
        temp = fTemp->readAll();
        temp.remove(temp.size() - 1, 1);
        tTemp = temp.toUInt();
        if(tTemp > iTemp)
            iTemp = tTemp;
    }
    if(fTCore1->isOpen()) {
        fTCore1->seek(0);
        temp = fTCore1->readAll();
        temp.remove(temp.size() - 1, 1);
        tTemp = temp.toUInt();
        if(tTemp > iTemp)
            iTemp = tTemp;
    }
    if(fTCore2->isOpen()) {
        fTCore2->seek(0);
        temp = fTCore2->readAll();
        temp.remove(temp.size() - 1, 1);
        tTemp = temp.toUInt();
        if(tTemp > iTemp)
            iTemp = tTemp;
    }
    if(fTCore3->isOpen()) {
        fTCore3->seek(0);
        temp = fTCore3->readAll();
        temp.remove(temp.size() - 1, 1);
        tTemp = temp.toUInt();
        if(tTemp > iTemp)
            iTemp = tTemp;
    }
    if(fTCore4->isOpen()) {
        fTCore4->seek(0);
        temp = fTCore4->readAll();
        temp.remove(temp.size() - 1, 1);
        tTemp = temp.toUInt();
        if(tTemp > iTemp)
            iTemp = tTemp;
    }
    emit temperatureReady(iTemp);
    tmCheckTemp->start(1000);
}

bool Manager::openIOPort()
{
    if ((fhIOPort = open("/dev/port", O_RDWR)) >= 0) {
        return true;
    } else {
        if ((fhIOPort = open("/dev/port", O_RDONLY)) >= 0){
            emit error(tr("Error opening /dev/port in write mode! Run as root user!"));
            return true;
        } else {
            emit error(tr("Error opening /dev/port! Check if user is in kmem group!"));
            return false;
        }
    }
}

bool Manager::closeIOPort()
{
    if(fhIOPort >= 0)
        if(close(fhIOPort) == 0) {
            fhIOPort = 0;
            return true;
        }
    return false;
}

qint8 Manager::readByte(qint64 offset)
{
    if(lseek(fhIOPort, offset, SEEK_SET) < 0)
        return 0xFF;
    qint8 data[1];
    if(read(fhIOPort, data, 1) != 1)
        return 0xFF;
    return data[0];
}

bool Manager::writeByte(qint8 data, qint64 offset)
{
    qint8 buffer[1];
    buffer[0] = data;
    if(lseek(fhIOPort, offset, SEEK_SET) < 0){
        return false;
    }
    return (write(fhIOPort, buffer, 1) == 1);
}

bool Manager::waitUntilMaskIsValue(qint8 mask, qint8 value)
{
    int counter = 0;
    while(counter < 10000) {
        if(((readByte(0x6C)) & mask) == value){
            return false;
        }
        counter ++;
        QThread::msleep(1);
    }
    return true;
}

bool Manager::ecIntroSequence()
{
    if (waitUntilMaskIsValue(0x80, 0x00)) {
        return false;
    }
    readByte(0x68);
    if (waitUntilMaskIsValue(0x02, 0x00)) {
        return false;
    }
    return writeByte(0x59, 0x6C);
}

bool Manager::ecOutroSequence()
{
    readByte(0x68);
    if (waitUntilMaskIsValue(0x02, 0x00)) {
        return false;
    }
    return writeByte(0xFF, 0x6C);
}

bool Manager::updateEcRegisters()
{
    bool ret = false;
    int value = 0;
    bool registersChanged = false;
    if(openIOPort()) {
        if(ecIntroSequence()) {
            if(!waitUntilMaskIsValue(0x02, 0x00)) {
                for(qint64 index = 0x00; index < 0xFF; index++){
                    readByte(0x68);
                    if (waitUntilMaskIsValue(0x02, 0x00)) {
                        return false;
                    }
                    value = readByte(index);
                    if(eCRegisters.at(index) != value){
                        registersChanged = true;
                        eCRegisters[index] = value;
                    }
                }
            } else {
                emit error(tr("Error waiting for writing magic value!"));
            }
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        } else {
            emit error(tr("Error intro EC sequence!"));
            if(!ecOutroSequence()) {
                ret = false;
                emit error(tr("Error outro EC sequence!"));
            }
        }
        if(!closeIOPort()) {
            ret = false;
            emit error(tr("Error closing port!"));
        }
    } else {
        emit error(tr("Error opening port!"));
    }
    if (registersChanged)
        emit registerHasNewValue();
    return ret;
}

const QVector<qint8> &Manager::getECRegisters()
{
    return eCRegisters;
}
