/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ecmodel.h"
#include <QDebug>

ECModel::ECModel(const QVector<qint8> &data, QObject *parent)
    : QAbstractTableModel(parent),
      dataVector(data)
{
}

QVariant ECModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();
    switch (orientation) {
    case Qt::Horizontal :
        if(section >= 0x0 && section <= 0xF){
            QString val;
            if(section <= 9)
                val.append(section + '0');
            else
                val.append(section - 10 + 'A');
            return val;
        } else {
            return QVariant();
        }
        break;
    case Qt::Vertical :
        if(section >= 0x0 && section <= 0xF){
            QString val;
            if(section <= 9)
                val.append(section + '0').append("0");
            else
                val.append(section - 10 + 'A').append("0");
            return val;
        } else {
            return QVariant();
        }
        break;
    default:
            return QVariant();
    }
}

int ECModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 16;
}

int ECModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 16;
}

QVariant ECModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    int indVec = index.row() * 16 + index.column();
    if(indVec > 0xFF || indVec >= dataVector.size())
        return QVariant();

    qint8 value = this->dataVector.at(indVec);

    if(role == Qt::DisplayRole) {
        return QByteArray(1, value).toHex();
    }
    if(role == Qt::EditRole)
        return QVariant(value);
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;
    return QVariant();
}

Qt::ItemFlags ECModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
