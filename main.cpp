/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mainwindow.h"

#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Prometheus-Technologies");
    QCoreApplication::setOrganizationDomain("perometheus.com");
    QCoreApplication::setApplicationName("QtAcerFanControl");

    QTranslator qtTranslator;
    qtTranslator.load("QtAcerFanControl_"+QLocale::system().name());
    a.installTranslator(&qtTranslator);

    MainWindow w;
    w.show();

    return a.exec();
}
