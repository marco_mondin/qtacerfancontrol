#-------------------------------------------------
#
# Project created by QtCreator 2016-11-27T15:56:48
#
#-------------------------------------------------

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtAcerFanControl
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    trayicon.cpp \
    manager.cpp \
    qled.cpp \
    ecmodel.cpp

HEADERS  += mainwindow.h \
    trayicon.h \
    manager.h \
    qled.h \
    ecmodel.h

RESOURCES += qled.qrc

FORMS    += mainwindow.ui
