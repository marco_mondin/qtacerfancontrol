/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QTimer>
#include <QFile>
#include <QString>
#include <QModelIndex>

#include "stdio.h"

class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(QObject *parent = 0);
    ~Manager();

    bool valid() const;

    const QVector<qint8> &getECRegisters();

signals:
    void temperatureReady(quint32);
    void error(QString);
    void registerHasNewValue();

public slots:
    bool setMaxSpeed();
    bool setNormalSpeed();

protected slots:
    void checkTemperature();

private:
    QTimer *tmCheckTemp;
    QFile *fTemp;
    QFile *fTCore1;
    QFile *fTCore2;
    QFile *fTCore3;
    QFile *fTCore4;

    int fhIOPort;

    bool mValid;
    bool started;

    bool openIOPort();
    bool closeIOPort();
    qint8 readByte(qint64 offset);
    bool writeByte(qint8 data, qint64 offset);
    bool waitUntilMaskIsValue(qint8 mask, qint8 value);
    bool ecIntroSequence();
    bool ecOutroSequence();

    bool updateEcRegisters();
    QVector<qint8> eCRegisters;
};

#endif // MANAGER_H
