/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ecmodel.h"
#include "manager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void resizeEvent(QResizeEvent *event);
    void showEvent(QShowEvent *event);

protected slots:
    void tempChanged(quint32 value);
    void processErrorManager(QString error);
private:
    Ui::MainWindow *ui;

    Manager *manager;
    ECModel *ecModel;

    void resizeTable();
};

#endif // MAINWINDOW_H
