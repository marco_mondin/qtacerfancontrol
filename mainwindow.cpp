/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of QtAcerFanControl.

QtAcerFanControl is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QtAcerFanControl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QtAcerFanControl.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    manager = new Manager(this);
    ecModel = new ECModel(manager->getECRegisters(), this);
    ui->TableViewECRegisters->setModel(ecModel);
    connect(manager, SIGNAL(registerHasNewValue()), ecModel, SIGNAL(modelReset()));
    connect(manager, SIGNAL(temperatureReady(quint32)), this, SLOT(tempChanged(quint32)));
    connect(manager, SIGNAL(error(QString)), this, SLOT(processErrorManager(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    resizeTable();
    QMainWindow::resizeEvent(event);
}

void MainWindow::showEvent(QShowEvent *event)
{
    resizeTable();
    QMainWindow::showEvent(event);
}

void MainWindow::tempChanged(quint32 value)
{
    int real = value / 1000;
    ui->progressBarTemp->setValue(real);
    repaint();
    if(ui->radioButtonAuto->isChecked()) {
        if(real >= ui->spinBoxHigh->value()) {
            manager->setMaxSpeed();
            ui->ledFan->setValue(true);
        }
        if(real <= ui->spinBoxLow->value()) {
            manager->setNormalSpeed();
            ui->ledFan->setValue(false);
        }
    }
    if(ui->radioButtonMax->isChecked() && !ui->ledFan->value()) {
        manager->setMaxSpeed();
        ui->ledFan->setValue(true);
    }
    if(ui->radioButtonNormal->isChecked() && ui->ledFan->value()) {
        manager->setNormalSpeed();
        ui->ledFan->setValue(false);
    }

    QByteArray monitor;
    qint8 position = ui->spinBoxMonitor->value();
    for(int index = 0; index < ui->spinBoxBytes->value(); index++)
        if((position + index) < manager->getECRegisters().size())
            monitor.append(QByteArray(1, manager->getECRegisters().at(position + index)).toHex());
    ui->lineEditValHex->setText(monitor);

    switch (ui->spinBoxBytes->value()) {
    case 1 :
        ui->lineEditValDec->setText(QByteArray::number(quint8(monitor.toUInt(NULL, 16))));
        ui->lineEditValDecSig->setText(QByteArray::number(qint8(monitor.toUInt(NULL, 16))));
        break;
    case 2 :
        ui->lineEditValDec->setText(QByteArray::number(quint16(monitor.toUInt(NULL, 16))));
        ui->lineEditValDecSig->setText(QByteArray::number(qint16(monitor.toUInt(NULL, 16))));
        break;
    case 3 :
        ui->lineEditValDec->setText(QByteArray::number(quint32(monitor.toUInt(NULL, 16))));
        ui->lineEditValDecSig->setText(QByteArray::number(qint32(monitor.toUInt(NULL, 16))));
        break;
    case 4 :
        ui->lineEditValDec->setText(QByteArray::number(quint32(monitor.toUInt(NULL, 16))));
        ui->lineEditValDecSig->setText(QByteArray::number(qint32(monitor.toUInt(NULL, 16))));
        break;
    }
}

void MainWindow::processErrorManager(QString error)
{
    ui->statusBar->showMessage(error, 5000);
    qDebug()<<error;
}

void MainWindow::resizeTable()
{
    int sizeW = (ui->TableViewECRegisters->width() - ui->TableViewECRegisters->verticalHeader()->width()) / ecModel->columnCount();
    int sizeH = (ui->TableViewECRegisters->height() - ui->TableViewECRegisters->horizontalHeader()->height()) / ecModel->rowCount();
    for (int index = 0; index < ecModel->columnCount(); index++)
        ui->TableViewECRegisters->setColumnWidth(index, sizeW);
    for (int index = 0; index < ecModel->rowCount(); index++)
        ui->TableViewECRegisters->setRowHeight(index, sizeH);
}
